import org.openqa.selenium.*;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.htmlelements.element.TextBlock;

import java.util.concurrent.TimeUnit;

@Title("Автотест balka-book.com")
public class BalkaBookTest {

    static final String baseUrl = "http://balka-book.com";
    static final String emailAdress = "allure.w.test@gmail.com";
    static final String password = "ltz9z255hy";


    @Step("1. Запускается браузер, разварачивается окно на весь экран и открывается сайт " + baseUrl)
    public static void openMainPage() {

        StartFirefox.driver.get(baseUrl);
        StartFirefox.driver.manage().window().maximize();
        StartFirefox.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);



    }

    @Step("2. Нажимает на кнопку " + " Личный кабинет")
    public static void clickPersonalAccountButton() {

        WebElement source = StartFirefox.driver.findElement(By.xpath(".//*[@id='topmenu']/ul/li[5]/a"));
        Assert.assertEquals("Личны кабинет", source.getText());
        StartFirefox.driver.findElement(By.xpath(".//*[@id='topmenu']/ul/li[5]/a")).click();
    }

    @Step("3. Заполняет поле Логин: " + emailAdress + "  .Пароль: " + password + " . нажимает кнопку - Войти")
    public static void enterValidDataAndClickEnterButton() {

        StartFirefox.driver.findElement(By.name("login")).sendKeys(emailAdress);
        StartFirefox.driver.findElement(By.xpath(".//*[@id='colfull']/div[2]/section[1]/div/form/div[2]/div[2]/input")).sendKeys(password);
        StartFirefox.driver.findElement(By.xpath(".//*[@id='colfull']/div[2]/section[1]/div/form/span")).click();

    }

    @Attachment("Скрины о проделанной работе :)")
    public static byte[] makeScreenshot() {

        return ((TakesScreenshot) StartFirefox.driver).getScreenshotAs(OutputType.BYTES);


    }
}