import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;


public class AutoTest {

    @Title("Balka Test")
    @Test
    public void balkaTest() throws InterruptedException{

        BalkaBookTest.openMainPage();
        BalkaBookTest.clickPersonalAccountButton();
        BalkaBookTest.enterValidDataAndClickEnterButton();
        Thread.sleep(1000);
        BalkaBookTest.makeScreenshot();



    }

}
